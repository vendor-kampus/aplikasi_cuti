<?php
	include("sess_check.php");
	
	// deskripsi halaman
	$pagedesc = "Persetujuan Cuti";
	$menuparent = "Persetujuan";
	include("layout_top.php");
	$now = date('Y-m-d');
	$Sql = "SELECT cuti.*, employee.* FROM cuti, employee WHERE cuti.id_pegawai=employee.id_pegawai AND cuti.no_cuti='$_GET[no]'";
	$Qry = mysqli_query($conn, $Sql);
	$data = mysqli_fetch_array($Qry);
 	$tahun = substr($data['nip'], 8,4);
	$thn = date('Y'); 
	$masakerja = $thn - $tahun;
?>
<script type="text/javascript">
$(document).ready(function() {
    $('#aksi').change(function(){
        if($(this).val() === '2'){ 
            $('#reject').attr('disabled', false);
        }else{
            $('#reject').attr('disabled', 'disabled');
        }
    });

});
</script>
<!-- top of file -->
		<!-- Page Content -->
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Data Approval Cuti</h1>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->

				<div class="row">
					<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal" name="cuti" action="approval_update.php" method="POST" enctype="multipart/form-data" onSubmit="return valid();">
							<div class="panel panel-default">
								<div class="panel-heading"><h3>Review Pengajuan Cuti</h3></div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-sm-3">No. Cuti</label>
										<div class="col-sm-4">
											<input type="text" name="no_cuti" class="form-control" value="<?php echo $data['no_cuti'];?>" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">NIP</label>
										<div class="col-sm-4">
											<input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $data['id_pegawai'];?> " readonly>
											<input type="hidden" name="jml_cuti" class="form-control" value="<?php echo $data['jml_cuti'];?> " readonly>
											<input type="text" name="nip" class="form-control" value="<?php echo $data['nip'];?> " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Nama Pemohon</label>
										<div class="col-sm-4">
											<input type="text" name="mulai" class="form-control" value="<?php echo $data['nama_emp'];?> " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Masa Kerja</label>
										<div class="col-sm-4">
											<input type="text" name="mulai" class="form-control" value="<?php echo $masakerja;?> Tahun " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Jenis Cuti</label>
										<div class="col-sm-4">
											<input type="text" name="jenis_cuti" class="form-control" value="<?php echo $data['jenis_cuti'];?>" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Tanggal Pengajuan</label>
										<div class="col-sm-4">
											<input type="text" name="mulai" class="form-control" value="<?php echo Indonesia2Tgl($data['tgl_pengajuan']);?> " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Tanggal Mulai</label>
										<div class="col-sm-4">
											<input type="text" name="mulai" class="form-control" value="<?php echo Indonesia2Tgl($data['tgl_awal']);?> " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Tanggal Akhir</label>
										<div class="col-sm-4">
											<input type="text" name="mulai" class="form-control" value="<?php echo Indonesia2Tgl($data['tgl_akhir']);?> " readonly>
										</div>
									</div>
									<div class="form-group"> 
										<label class="control-label col-sm-3">Lama Cuti (Hari)</label>
										<div class="col-sm-4">
											<input type="text" name="lama_cuti" class="form-control" value="<?php echo $data['lama_cuti'];?> " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Unit Kerja</label>
										<div class="col-sm-4">
											<input type="text" name="mulai" class="form-control" value="<?php echo $data['unit_kerja'];?> " readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Keterangan</label>
										<div class="col-sm-4">
											<textarea name="keterangan" class="form-control" placeholder="Keterangan" rows="3" readonly><?php echo $data['keterangan'];?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Aksi</label>
										<div class="col-sm-4">
											<select name="aksi" id="aksi" class="form-control" required>
												<option value="" selected>---- Pilih Aksi ----</option>
												<option value="1">Disetujui</option>
												<option value="2">Tidak Disetujui</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">NIP TTD</label>
										<div class="col-sm-4">
											<input type="number" name="nip_ttd" id="nip_ttd" class="form-control" required="">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Nama TTD</label>
										<div class="col-sm-4">
											<input type="text" name="nama_ttd" id="nama_ttd" class="form-control" required="">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Keterangan Tidak Disetujui</label>
										<div class="col-sm-4">
											<textarea name="reject" id="reject" class="form-control" placeholder="Keterangan Tidak Disetujui" rows="3" disabled></textarea>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<button type="submit" name="simpan" class="btn btn-success">Simpan</button>
								</div>
							</div><!-- /.panel -->
						</form>
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
	include("layout_bottom.php");
?>