<?php
	include("sess_check.php");
	
	// deskripsi halaman
	$pagedesc = "Buat Pengajuan";
	$menuparent = "cuti";
	include("layout_top.php");
	$now = date('Y-m-d'); 
	$id_pegawai = $sess_pegawaiid;
?>
<script type="text/javascript">
function valid()
{ 
	if(document.cuti.tgl_akhir.value < document.cuti.tgl_awal.value){
		alert("Tanggal akhir harus lebih besar dari tanggal mulai cuti!");
		return false;
	}
	 
	return true;
}
</script>
<!-- top of file -->
	<script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
		<!-- Page Content -->
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Pengajuan Cuti Sakit</h1>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
                <label>Lihat Peraturan Pengajuan Cuti Sakit</label>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSaya">
  				 Lihat
				</button>
				<hr />
				<div class="row">
					<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal" name="cuti" action="cuti_insert.php" method="POST" enctype="multipart/form-data" onSubmit="return valid();">
							<div class="panel panel-default">
								<div class="panel-heading"><h3>Form Pengajuan Cuti Sakit</h3></div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-sm-3">NIP</label>
										<div class="col-sm-4">
											<input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $id_pegawai;?>"readonly>
											<input type="text" name="nip" class="form-control" value="<?php echo $res['nip'];?>"readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Jenis Cuti</label>
										<div class="col-sm-4">
											<input type="text" name="jenis_cuti" class="form-control" value="Cuti Sakit" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Mulai Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_awal" class="form-control" required>
											<input type="hidden" name="now" class="form-control" value="<?php echo $now;?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Akhir Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_akhir" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Alamat Selama Menjalankan Cuti</label>
										<div class="col-sm-8">
											<input type="text" name="alm_cuti" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Keterangan</label>
										<div class="col-sm-8">
											<input type="text" name="keterangan" class="form-control" required>
										</div>
									</div>
									<input type="hidden" name="stt_cuti" class="form-control" value="Menunggu Persetujuan">
									<input type="hidden" name="ket_reject" class="form-control" value="">
									<input type="hidden" name="hrd_app" class="form-control" value="0">
								</div>
								<div class="panel-footer">
									<button type="submit" name="simpan" class="btn btn-success">Simpan</button>
								</div>
							</div><!-- /.panel -->
						</form>
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->

<!-- Modal -->
<div class="modal fade" id="modalSaya" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSayaLabel" align="center"><b>Peraturan Pengajuan Cuti Sakit</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="justify">
<table width="100%" align="justify" class="table table-striped table-bordered table-hover">
<tr>
<td width="2%">1.</td>
<td width="98%">Setiap PNS yang menderita sakit berhak atas cuti sakit. </td>
</tr>
<tr>
<td width="2%">2.</td>
<td width="98%">PNS yang sakit 1 (satu) hari menyampaikan surat keterangan sakit secara tertulis kepada atasan langsung dengan melampirkan surat keterangan dokter. </td>
</tr>
<tr>
<td width="2%">3.</td>
<td width="98%">PNS yang sakit lebih dari 1 (satu) hari sampai dengan 14 (empat belas) hari berhak atas cuti sakit, dengan ketentuan bahwa PNS yang bersangkutan harus mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti dengan melampirkan surat keterangan dokter. </td>
</tr>
<tr>
<td width="2%">4.</td>
<td width="98%">PNS yang menderita sakit lebih dari 14 (empat belas) hari berhak atas cuti sakit, dengan ketentuan bahwa PNS yang bersangkutan harus mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti dengan melampirkan surat keterangan dokter pemerintah. </td>
</tr>
<tr>
<td width="2%">5.</td>
<td width="98%">Dokter pemerintah sebagaimana dimaksud dalam angka 4 merupakan dokter yang berstatus PNS atau dokter yang bekerja pada unit pelayanan kesehatan pemerintah. </td>
</tr>
<tr>
<td width="2%">6.</td>
<td width="98%">Surat Keterangan dokter sebagaimana dimaksud pada angka 3 dan 4 paling sedikit memuat pernyataan tentang perlunya diberikan cuti, lamanya cuti dan keterangan lain yang diperlukan. </td>
</tr>
<tr>
<td width="2%">7.</td>
<td width="98%">Hak atas cuti sakit diberikan untuk waktu paling lama 1 (satu) tahun. </td>
</tr>
<tr>
<td width="2%">8.</td>
<td width="98%">Jangka waktu cuti sakit sebagaimana dimaksud pada angka 7 dapat ditambah untuk paling lama 6 (enam) bulan apabila diperlukan,  berdasarkan surat keterangan tim penguji kesehatan yang ditetapkan oleh menteri yang menyelenggarakan urusan pemerintahan di bidang kesehatan. </td>
</tr>
<tr>
<td width="2%">9.</td>
<td width="98%">PNS yang tidak sembuh dari penyakitnya dalam jangka waktu sebagaimana dimaksud pada angka 8, harus diuji kembali kesehatannya oleh tim penguji kesehatan yang ditetapkan oleh menteri yang menyelenggarakan urusan pemerintahan di bidang kesehatan. </td>
</tr>
<tr>
<td width="2%">10.</td>
<td width="98%">Apabila berdasarkan hasil pengujian kesehatan sebagaimana dimaksud pada angka 9 PNS belum sembuh dari penyakitnya, PNS yang bersangkutan diberhentikan dengan hormat dari jabatannya karena sakit dengan mendapat uang tunggu sesuai dengan ketentuan peraturan perundang-undangan.  </td>
</tr>
<tr>
<td width="2%">11.</td>
<td width="98%">PNS yang mengalami gugur kandungan berhak atas cuti sakit untuk paling lama 1,5 (satu setengah) bulan. </td>
</tr>
<tr>
<td width="2%">12.</td>
<td width="98%">Untuk menggunakan hak atas cuti sakit sebagaimana dimaksud pada angka 1, PNS yang bersangkutan mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti. </td>
</tr>
<tr>
<td width="2%">13.</td>
<td width="98%">Berdasarkan permintaan secara tertulis sebagaimana dimaksud pada angka 12, Pejabat Yang Berwenang Memberikan Cuti memberikan cuti sakit kepada PNS yang bersangkutan. </td>
</tr>
<tr>
<td width="2%">14.</td>
<td width="98%">Permintaan dan pemberian cuti sakit sebagaimana  dimaksud pada angka 12 dan angka 13 dibuat menurut contoh dengan menggunakan formulir sebagaimana tercantum dalam Anak Lampiran 1.b yang merupakan bagian tidak terpisahkan dari Peraturan Badan ini. </td>
</tr>
<tr>
<td width="2%">15.</td>
<td width="98%">PNS yang mengalami kecelakaan dalam dan oleh karena menjalankan tugas kewajibannya sehingga yang bersangkutan perlu mendapat perawatan berhak atas cuti sakit sampai yang bersangkutan sembuh dari penyakitnya. </td>
</tr>
<tr>
<td width="2%">16.</td>
<td width="98%">Selama menjalankan cuti sakit, PNS yang bersangkutan menerima penghasilan PNS. </td>
</tr>
<tr>
<td width="2%">17.</td>
<td width="98%">Penghasilan sebagaimana dimaksud pada angka 16, terdiri atas gaji pokok, tunjangan keluarga, tunjangan pangan dan tunjangan jabatan sampai dengan ditetapkannya Peraturan Pemerintah yang mengatur gaji, tunjangan, dan fasilitas PNS. </td>
</tr>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>	
            </div><!-- /.container-fluid -->
        </div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
	include("layout_bottom.php");
?>