<?php
	include("sess_check.php");
	
	// deskripsi halaman
	$pagedesc = "Buat Pengajuan";
	$menuparent = "cuti";
	include("layout_top.php");
	$now = date('Y-m-d'); 
	$id_pegawai = $sess_pegawaiid;
?>
<script type="text/javascript">
function valid()
{ 
	if(document.cuti.tgl_akhir.value < document.cuti.tgl_awal.value){
		alert("Tanggal akhir harus lebih besar dari tanggal mulai cuti!");
		return false;
	}
	 
	return true;
}
</script>
<!-- top of file -->
	<script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
		<!-- Page Content -->
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Pengajuan Cuti Karena Alasan Penting</h1>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
				<label>Lihat Peraturan Pengajuan Cuti Karena Alasan Penting</label>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSaya">
  				 Lihat
				</button>
				<hr />
				<div class="row">
					<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal" name="cuti" action="cuti_insert.php" method="POST" enctype="multipart/form-data" onSubmit="return valid();">
							<div class="panel panel-default">
								<div class="panel-heading"><h3>Form Pengajuan Cuti karena Alasan Penting</h3></div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-sm-3">NIP</label>
										<div class="col-sm-4">
											<input type="text" name="nip" class="form-control" value="<?php echo $nip;?>"readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Jenis Cuti</label>
										<div class="col-sm-4">
											<input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $id_pegawai;?>"readonly>
											<input type="text" name="nip" class="form-control" value="<?php echo $res['nip'];?>"readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Mulai Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_awal" class="form-control" required>
											<input type="hidden" name="now" class="form-control" value="<?php echo $now;?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Akhir Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_akhir" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Alamat Selama Menjalankan Cuti</label>
										<div class="col-sm-8">
											<input type="text" name="alm_cuti" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Keterangan</label>
										<div class="col-sm-8">
											<input type="text" name="keterangan" class="form-control" required>
										</div>
									</div>
									<input type="hidden" name="stt_cuti" class="form-control" value="Menunggu Persetujuan">
									<input type="hidden" name="ket_reject" class="form-control" value="">
									<input type="hidden" name="hrd_app" class="form-control" value="0">
								</div>
								<div class="panel-footer">
									<button type="submit" name="simpan" class="btn btn-success">Simpan</button>
								</div>
							</div><!-- /.panel -->
						</form>
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalSaya" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSayaLabel" align="center"><b>Peraturan Pengajuan Cuti karena alasan penting</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="justify">
<table width="100%" align="justify" class="table table-striped table-bordered table-hover">
<tr>
<td width="2%">1.</td>
<td width="98%">PNS berhak atas cuti karena alasan penting, apabila:
<br />
a. ibu, bapak, isteri atau suami, anak, adik, kakak, mertua, atau menantu sakit keras atau meninggal dunia;
<br />
b. salah seorang anggota keluarga yang dimaksud pada huruf a meninggal dunia, dan menurut peraturan perundang- undangan PNS yang bersangkutan harus mengurus hak- hak dari anggota keluarganya yang meninggal dunia; atau
<br />
c. melangsungkan perkawinan.
</td>
</tr>
<tr>
<td width="2%">2.</td>
<td width="98%">Sakit keras sebagaimana dimaksud pada angka 1 huruf a dibuktikan dengan melampirkan surat keterangan rawat inap dari Unit Pelayanan Kesehatan.</td>
</tr>
<tr>
<td width="2%">3.</td>
<td width="98%">PNS laki-laki yang isterinya melahirkan/operasi caesar dapat diberikan cuti karena alasan penting dengan melampirkan surat keterangan rawat inap dari Unit Pelayanan Kesehatan.</td>
</tr>
<tr>
<td width="2%">4.</td>
<td width="98%">Dalam hal PNS mengalami musibah kebakaran rumah atau bencana alam, dapat diberikan cuti karena alasan penting dengan melampirkan surat keterangan paling rendah dari Ketua Rukun Tetangga.</td>
</tr>
<tr>
<td width="2%">5.</td>
<td width="98%">PNS yang ditempatkan pada perwakilan Republik Indonesia yang rawan dan/ atau berbahaya dapat mengajukan cuti karena alasan penting guna memulihkan kondisi kejiwaan PNS yang bersangkutan.</td>
</tr>
<tr>
<td width="2%">6.</td>
<td width="98%">Lamanya cuti karena alasan penting ditentukan oleh Pejabat Yang Berwenang Memberikan Cuti paling lama 1 (satu) bulan.</td>
</tr>
<tr>
<td width="2%">7.</td>
<td width="98%">Untuk menggunakan hak atas cuti karena alasan penting sebagaimana dimaksud pada angka 1, PNS yang bersangkutan mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti.</td>
</tr>
<tr>
<td width="2%">8.</td>
<td width="98%">Berdasarkan permintaan secara tertulis sebagaimana dimaksud pada angka 7, Pejabat Yang Berwenang Memberikan Cuti memberikan cuti karena alasan penting kepada PNS yang bersangkutan.</td>
</tr>
<tr>
<td width="2%">9.</td>
<td width="98%">Permintaan dan pemberian cuti karena alasan penting sebagaimana dimaksud pada angka 7 dan angka 8 dibuat menurut contoh dengan menggunakan formulir sebagaimana tercantum dalam Anak Lampiran 1.b yang merupakan bagian tidak terpisahkan dari Peraturan Badan ini.</td>
</tr>
<tr>
<td width="2%">10.</td>
<td width="98%">Dalam hal yang mendesak, sehingga PNS yang bersangkutan tidak dapat menunggu keputusan dari Pejabat Yang Berwenang Memberikan Cuti, pejabat yang tertinggi di tempat PNS yang bersangkutan bekerja dapat memberikan izin sementara secara tertulis untuk menggunakan hak atas cuti karena alasan penting.</td>
</tr>
<tr>
<td width="2%">11.</td>
<td width="98%">Pejabat sebagaimana yang dimaksud pada angka 10 dapat memberikan izin sementara secara tertulis menurut contoh sebagaimana tercantum dalam Anak Lampiran 1.c yang merupakan bagian tidak terpisahkan dari Peraturan Badan ini.</td>
</tr>
<tr>
<td width="2%">12.</td>
<td width="98%">Pemberian izin sementara sebagaimana dimaksud pada angka 10 harus segera diberitahukan kepada Pejabat Yang Berwenang Memberikan Cuti.</td>
</tr>
</tr>
<tr>
<td width="2%">13.</td>
<td width="98%">Pejabat Yang Berwenang Memberikan Cuti setelah menerima pemberitahuan sebagaimana dimaksud pada angka 12 memberikan hak atas cuti karena alasan penting kepada PNS yang bersangkutan.</td>
</tr>
</tr>
<tr>
<td width="2%">14.</td>
<td width="98%">Selama menggunakan hak atas cuti karena alasan penting, PNS yang bersangkutan menerima penghasilan PNS.</td>
</tr>
</tr>
<tr>
<td width="2%">15.</td>
<td width="98%">Penghasilan sebagaimana dimaksud pada angka 14, terdiri atas gaji pokok, tunjangan keluarga, tunjangan pangan, dan tunjangan jabatan sampai dengan ditetapkannya Peraturan Pemerintah yang mengatur gaji, tunjangan, dan fasilitas PNS.</td>
</tr>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>	
            </div><!-- /.container-fluid -->
        </div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
	include("layout_bottom.php");
?>