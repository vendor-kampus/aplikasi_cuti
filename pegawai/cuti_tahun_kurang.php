<?php
	include("sess_check.php");
	
	// deskripsi halaman
	$pagedesc = "Buat Pengajuan";
	$menuparent = "cuti";
	include("layout_top.php");
	$now = date('Y-m-d');
	$nip = $sess_pegawaiid;
?>
<script type="text/javascript">
function valid()
{ 
  if(document.cuti.tgl_akhir.value < document.cuti.tgl_awal.value){
    alert("Tanggal akhir harus lebih besar dari tanggal mulai cuti!");
    return false;
  }

  if(document.cuti.tgl_awal.value < document.cuti.now.value){
    alert("Tanggal mulai cuti tidak valid!");
    return false;
  }
   
  return true;
}
</script>

<!-- top of file --> 
  <head>
    <script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
</head>
		<!-- Page Content -->
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Pengajuan Cuti Tahunan</h1>
                        <hr/>

                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
                
                        <label>Lihat Peraturan Pengajuan Cuti Tahunan</label>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSaya">
  Lihat
</button>
<hr />

</div>
  <!-- Tombol untuk menampilkan modal-->
  
				<div class="row">
					<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<?php
if(isset($_POST['simpan'])){
$no_cuti        	= $_POST['no_cuti'];
$nip          		= $_POST['nip'];
$now              = date('Y-m-d');
$tgl_awal      		= $_POST['tgl_awal'];
$tgl_akhir    		= $_POST['tgl_akhir'];
$jenis_cuti       = $_POST['jenis_cuti'];
$keterangan       = $_POST['keterangan'];
$alm_cuti         = $_POST['alm_cuti'];
$stt_cuti         = $_POST['stt_cuti'];
$nip_ttd          = $_POST['nip_ttd'];
$nama_ttd         = $_POST['nama_ttd'];
$ket_reject       = $_POST['ket_reject'];
$hrd_app          = $_POST['hrd_app'];

$start = new DateTime($tgl_awal);
$finish = new DateTime($tgl_akhir);
$int = $start->diff($finish);
$dur = $int->days;
$lama_cuti = $dur+1;

$sql = mysqli_query($conn, "SELECT * FROM employee WHERE nip='$nip'");
             if(mysqli_num_rows($sql) == 0){
				header("Location: cuti_create_thn.php");
			}else{
				$row = mysqli_fetch_assoc($sql);
            }
            
$nip = $row['nip'];
$jml_cuti = $row['jml_cuti'];

            if ($jml_cuti == 0) {
                echo "<script>alert('cuti $nip sudah habis, tidak bisa membuat cuti!'); window.location = 'cuti_tahun.php'</script>";
                  } else if ($jml_cuti <= 0) {
                    echo "<script>alert('cuti $nip sudah habis, tidak bisa membuat cuti!'); window.location = 'cuti_tahun_kurang.php'</script>";
                      } else {

$query = mysqli_query($conn, "INSERT INTO cuti (no_cuti, nip, tgl_pengajuan, tgl_awal, tgl_akhir, lama_cuti, jenis_cuti, keterangan, alm_cuti, stt_cuti, nip_ttd, nama_ttd, ket_reject, hrd_app) VALUES ('$no_cuti', '$nip', '$now', '$tgl_awal', '$tgl_akhir', '$lama_cuti', '$jenis_cuti', '$keterangan', '$alm_cuti', '$stt_cuti', '$nip_ttd', '$nama_ttd', '$ket_reject', '$hrd_app')");

  if($stt_cuti="Disetujui") {
    $qu	   = mysqli_query($conn, "UPDATE employee SET jml_cuti=(jml_cuti-'$lama_cuti')WHERE nip='$nip'");
      }
  else if($stt_cuti="Menunggu Persetujuan") {
    $qu    = mysqli_query($conn, "UPDATE employee SET jml_cuti=$jml_cuti WHERE nip='$nip'");
      }

if ($query&&$qu){
    echo "<script>alert('cuti $nip berhasil di buat!'); window.location = 'cuti_tahun_kurang.php'</script>";
	//echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data berhasil disimpan.</div>';
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data gagal disimpan, silahkan coba lagi.</div>';
                
            }}
}

                ?>
                <div class="box-body">
                  <div class="form-panel">
                      <form class="form-horizontal style-form" action="cuti_tahun_kurang.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                  <input name="no_cuti" type="hidden" id="no_cuti" class="form-control"  value="" readonly="readonly" />

                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Jenis Cuti</label>
                            <div class="col-sm-4">
                              <input type="input" name="jenis_cuti" class="form-control" value="Cuti Tahunan" readonly>
                            </div>
                          </div>        
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">NIP</label>
                              <div class="col-sm-4">
                              <input type="input" name="nip" class="form-control" value="<?php echo $nip;?>" readonly>
                            </div>
                          </div>
                          <input type="hidden" name="now" class="form-control" value="<?php echo $now;?>" required>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Tanggal Awal Cuti</label>
                              <div class="col-sm-4">
                              <input type='date' class="input-group date form-control" data-date="" data-date-format="yyyy-mm-dd" name='tgl_awal' id="tgl_awal" placeholder='Tanggal Awal' autocomplete='off' required='required' />
                              
                            </div>
                          </div>
                           <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Tanggal Akhir Cuti</label>
                              <div class="col-sm-4">
                              <input type='date' class="input-group date form-control" data-date="" data-date-format="yyyy-mm-dd" name='tgl_akhir' id="tgl_akhir" placeholder='Tanggal Awal' autocomplete='off' required='required' />
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Alamat Selama Menjalankan Cuti</label>
                              <div class="col-sm-8">
                                  <input name="alm_cuti" type="text" id="alm_cuti" class="form-control" placeholder="Alamat" autocomplete="off" required />
                              </div>
                          </div> 


                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                              <div class="col-sm-8">
                                  <input name="keterangan" type="text" id="keterangan" class="form-control" placeholder="Keterangan" autocomplete="off" required />

                              </div>
                          </div>
                          <div class="form-group">
                    <label class="control-label col-sm-3">Persetujuan</label>
                    <div class="col-sm-4">
                      <select name="stt_cuti" id="stt_cuti" class="form-control" required>
                      <option value="" selected>Pilih Persetujuan</option>
                      <option value="Cuti Tahunan">Menunggu Persetujuan</option>
                      <option value="Cuti Besar">Disetujui</option>
                      <option value="Cuti Sakit">Tidak Disetujui</option>
                      </select>
                    </div>
                  </div>
                          
                          <input type="hidden" name="ket_reject" class="form-control" value="" required>
                          <input type="hidden" name="nip_ttd" class="form-control" value="" required>
                          <input type="hidden" name="nama_ttd" class="form-control" value="" required>
                          <div class="form-group">
                          <input type="hidden" name="hrd_app" class="form-control" value="0" required>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label"></label>
                              <div class="col-sm-10">
                                  <input type="submit" name="simpan" value="Simpan" class="btn btn-sm btn-primary" />&nbsp;
                              </div>
                          </div>
                      </form>
                  </div>
                </div><!-- /.box-body -->
                <!-- <div class="box-footer clearfix no-border">
                  <a href="#" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Tambah Admin</a>
                </div> -->
              </div><!-- /.box -->

            </section><!-- /.Left col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->
        <!-- Modal -->
<div class="modal fade" id="modalSaya" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSayaLabel" align="center"><b>Peraturan Pengajuan Cuti Tahunan</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="justify">
<table width="100%" align="justify" class="table table-striped table-bordered table-hover">
<tr>
<td width="2%">1.</td>
<td width="98%">PNS dan Calon PNS yang telah bekerja paling kurang 1 (satu) tahun secara terus menerus berhak atas cuti tahunan.</td>
</tr>
<tr>
<td width="2%">2.</td>
<td width="98%">Lamanya hak atas cuti tahunan sebagaimana dimaksud pada angka 1 adalah 12 (dua belas) hari kerja.</td>
</tr>
<tr>
<td width="2%">3.</td>
<td width="98%">Permintaan cuti tahunan dapat diberikan untuk paling kurang 1 (satu) hari kerja.</td>
</tr>
<tr>
<td width="2%">4.</td>
<td width="98%">Untuk menggunakan hak atas cuti tahunan sebagaimana dimaksud pada angka 1, PNS atau Calon PNS yang bersangkutan mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti.</td>
</tr>
<tr>
<td width="2%">5.</td>
<td width="98%">Berdasarkan permintaan secara tertulis sebagaimana dimaksud pada angka 4, Pejabat Yang Berwenang Memberikan Cuti memberikan cuti tahunan kepada PNS atau Calon PNS yang bersangkutan.</td>
</tr>
<tr>
<td width="2%">6.</td>
<td width="98%">Permintaan dan pemberian cuti sebagaimana dimaksud pada angka 4 dan angka 5 dibuat menurut contoh dengan menggunakan formulir sebagaimana tercantum dalam Anak Lampiran 1.b yang merupakan bagian tidak terpisahkan dari Peraturan Badan ini.</td>
</tr>
<tr>
<td width="2%">7.</td>
<td width="98%">Dalam hal hak atas cuti tahunan sebagaimana dimaksud pada angka 1 yang akan digunakan di tempat yang sulit perhubungannya maka jangka waktu cuti tahunan tersebut dapat ditambah untuk paling lama 12 (dua belas) hari kalender.</td>
</tr>
<tr>
<td width="2%">8.</td>
<td width="98%">Hak atas cuti tahunan yang tidak digunakan dalam tahun yang bersangkutan, dapat digunakan dalam tahun berikutnya untuk paling lama 18 (delapan belas) hari kerja termasuk cuti tahunan dalam tahun berjalan.
<br />
Contoh:
<br />
Sdr. Heru Sudiyanto NIP. 196303121991021005 dalam tahun 2018 tidak mengajukan permintaan cuti tahunan. Pada tahun 2019 yang bersangkutan mengajukan permintaan cuti tahunan, untuk tahun 2018 dan tahun 2019. Dalam hal demikian maka Pejabat Yang Berwenang Memberikan Cuti hanya dapat memberikan cuti tahunan kepada PNS yang bersangkutan paling lama 18 (delapan belas) hari kerja.
</tr>
<tr>
<td width="2%">9.</td>
<td width="98%">Sisa hak atas cuti tahunan yang tidak digunakan dalam tahun bersangkutan dapat digunakan pada tahun berikutnya paling banyak 6 (enam) hari kerja.
<br />
Contoh:
a. Sdri. Dian Sulistiowati NIP. 198609222014022001, tahun 2018 menggunakan hak cuti tahunan selama 3 (tiga) hari kerja, sisa hak cuti tahunan Sdri. Dian Sulistiowati pada tahun 2018 sebanyak 9 (sembilan) hari kerja. Dalam hal demikian hak cuti tahunan yang dapat diperhitungkan untuk tahun 2019 sebanyak 18 (delapan belas) hari kerja, termasuk cuti tahunan dalam tahun 2019.
<br />
b. Sdri. Wening Wulandari NIP 197805262010052009, tahun 2018 menggunakan hak cuti tahunan selama 7 (tujuh) hari kerja, sisa hak cuti tahunan Sdri. Wening Wulandari pada tahun 2018 sebanyak 5 (lima) hari kerja. Dalam hal demikian hak cuti tahunan yang dapat diperhitungkan untuk tahun 2019 sebanyak 17 (tujuh belas) hari kerja.
</td>
</tr>
<tr>
<td width="2%">10.</td>
<td width="98%">Hak atas cuti tahunan sebagaimana dimaksud pada angka 1 yang tidak digunakan 2 (dua) tahun atau lebih berturut-turut, dapat digunakan dalam tahun berikutnya untuk paling lama 24 (dua puluh empat) hari kerja termasuk hak atas cuti tahunan dalam tahun berjalan.
<br />
Contoh:
<br />
a. Sdr. Saputra NIP. 198009252004021004 dalam tahun 2018 dan tahun 2019 tidak mengajukan permintaan cuti tahunan. Pada tahun 2020 yang bersangkutan mengajukan permintaan cuti tahunan untuk tahun 2018, 2019, dan 2020. Dalam hal demikian Pejabat Yang Berwenang Memberikan Cuti dapat memberikan cuti tahunan kepada PNS bersangkutan untuk paling lama 24 (dua puluh empat) hari kerja, termasuk cuti tahunan dalam tahun 2020.
<br />
b. Sdr. Agus Wahyudi NIP. 198505142014011001, tahun 2O17 menggunakan hak cuti tahunan selama 5 (lima) hari kerja. Pada tahun 2018, cuti tahunan tidak digunakan. Dalam hal demikian Pejabat Yang Berwenang Memberikan Cuti dapat memberikan cuti tahunan kepada PNS bersangkutan untuk paling lama 18 (delapan belas) hari kerja, termasuk cuti tahunan dalam tahun 2019.
<br />
c. Sdri. Fadzilla NIP. 198708112014022001 tahun 2018 menggunakan hak cuti tahunan selama 7 (tujuh) hari kerja. Pada tahun 2019, cuti tahunan yang bersangkutan tidak digunakan. Dalam hal demikian Pejabat Yang Berwenang Memberikan Cuti dapat memberikan cuti tahunan kepada PNS bersangkutan untuk paling lama 18 (delapan belas) hari kerja, termasuk cuti tahunan dalam tahun 2020.
</td>
</tr>
<tr>
<td width="2%">11.</td>
<td width="98%">Hak atas cuti tahunan dapat ditangguhkan penggunaannya oleh Pejabat Yang Berwenang Memberikan Cuti untuk paling lama 1 (satu) tahun, apabila terdapat kepentingan dinas mendesak.</td>
</tr>
<tr>
<td width="2%">12.</td>
<td width="98%">Hak atas cuti tahunan yang ditangguhkan sebagaimana dimaksud pada angka 11 dapat digunakan dalam tahun berikutnya selama 24 (dua puluh empat) hari kerja termasuk hak atas cuti tahunan dalam tahun berjalan.
<br />
Contoh:
<br />
Sdri. Sri Rahayu NIP. 199009252014022004 mengajukan permintaan cuti tahunan untuk tahun 2018 selama 12 (dua belas) hari kerja. Pejabat Yang Berwenang Memberikan Cuti tidak memberikan cuti karena kepentingan dinas mendesak. Dalam hal demikian maka hak atas cuti tahunan Sdri. Sri Rahayu pada tahun 2019 menjadi selama 24 (dua puluh empat) hari kerja, termasuk hak atas cuti tahunan dalam tahun berjalan.
</td>
</tr>
<tr>
<td width="2%">13.</td>
<td width="98%">Dalam hal terdapat PNS yang telah menggunakan Hak atas cuti tahunan dan masih terdapat sisa Hak atas cuti tahunan untuk tahun berjalan, dapat ditangguhkan penggunaannya oleh Pejabat Yang Berwenang Memberikan Cuti untuk tahun berikutnya, apabila terdapat kepentingan dinas mendesak.</td>
</tr>
<tr>
<td width="2%">14.</td>
<td width="98%">Hak atas sisa cuti tahunan yang ditangguhkan sebagaimana dimaksud pada angka 13 dihitung penuh dalam tahun berikutnya.
<br />
Contoh:
<br />
Sdr. Dicky Pamungkas NIP. 199009252014021004 memiliki sisa cuti tahunan pada tahun 2018 sebanyak 9 (sembilan) hari kerja. Pada akhir tahun 2018 yang bersangkutan mengajukan kembali permintaan cuti tahunan untuk tahun 2018 selama 9 (sembilan) hari kerja. Pejabat Yang Berwenang Memberikan Cuti menangguhkan hak atas cuti tahunan untuk tahun 2018 karena kepentingan dinas mendesak. Dalam hal demikian maka hak atas cuti tahunan Sdr. Dicky Pamungkas pada tahun 2019 menjadi selama 21 (dua puluh satu) hari kerja, termasuk hak atas cuti tahunan dalam tahun 2019.
</td>
</tr>
<tr>
<td width="2%">15.</td>
<td width="98%">PNS yang menduduki jabatan guru pada sekolah dan jabatan dosen pada perguruan tinggi yang mendapat liburan menurut peraturan perundang-undangan, disamakan dengan PNS yang telah menggunakan hak cuti tahunan.</td>
</tr>
<tr>
<td width="2%">16.</td>
<td width="98%">Pemberian cuti tahunan harus memperhatikan kekuatan jumlah pegawai pada unit kerja yang bersangkutan.</td>
</tr>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
            </div><!-- /.container-fluid -->
        </div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
	include("layout_bottom.php");
?>
