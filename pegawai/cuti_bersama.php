<?php
	include("sess_check.php");
	
	// deskripsi halaman
	$pagedesc = "Buat Pengajuan";
	$menuparent = "cuti";
	include("layout_top.php");
	$now = date('Y-m-d'); 
	$id_pegawai = $sess_pegawaiid; 
?>
<script type="text/javascript">
function valid()
{ 
	if(document.cuti.tgl_akhir.value < document.cuti.tgl_awal.value){
		alert("Tanggal akhir harus lebih besar dari tanggal mulai cuti!");
		return false;
	}
	 
	return true;
}
</script>
<!-- top of file -->
	<script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
		<!-- Page Content -->
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Pengajuan Cuti Bersama</h1>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
				<label>Lihat Peraturan Pengajuan Cuti Bersama</label>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSaya">
  				 Lihat
				</button>
				<hr />
				<div class="row">
					<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal" name="cuti" action="cuti_insert.php" method="POST" enctype="multipart/form-data" onSubmit="return valid();">
							<div class="panel panel-default">
								<div class="panel-heading"><h3>Form Pengajuan Cuti Bersama</h3></div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-sm-3">NIP</label>
										<div class="col-sm-4">
											<input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $id_pegawai;?>"readonly>
											<input type="text" name="nip" class="form-control" value="<?php echo $res['nip'];?>"readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Jenis Cuti</label>
										<div class="col-sm-4">
											<input type="text" name="jenis_cuti" class="form-control" value="Cuti Bersama" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Mulai Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_awal" class="form-control" required>
											<input type="hidden" name="now" class="form-control" value="<?php echo $now;?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Akhir Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_akhir" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Alamat Selama Menjalankan Cuti</label>
										<div class="col-sm-8">
											<input type="text" name="alm_cuti" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Keterangan</label>
										<div class="col-sm-8">
											<input type="text" name="keterangan" class="form-control" required>
										</div>
									</div>
									<input type="hidden" name="stt_cuti" class="form-control" value="Menunggu Persetujuan">
									<input type="hidden" name="ket_reject" class="form-control" value="">
									<input type="hidden" name="hrd_app" class="form-control" value="0">
								</div>
								<div class="panel-footer">
									<button type="submit" name="simpan" class="btn btn-success">Simpan</button>
								</div>
							</div><!-- /.panel -->
						</form>
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->
<!-- Modal -->
<div class="modal fade" id="modalSaya" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSayaLabel" align="center"><b>Peraturan Pengajuan Cuti Bersama</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="justify">
<table width="100%" align="justify" class="table table-striped table-bordered table-hover">
<tr>
<td width="2%">1.</td>
<td width="98%">Presiden dapat menetapkan cuti bersama.</td>
</tr>
<tr>
<td width="2%">2.</td>
<td width="98%">Cuti bersama sebagaimana dimaksud pada angka 1 tidak mengurangi hak cuti tahunan.</td>
</tr>
<tr>
<td width="2%">3.</td>
<td width="98%">Cuti bersama sebagaimana dimaksud pada angka 1 ditetapkan dengan Keputusan Presiden.</td>
</tr>
<tr>
<td width="2%">4.</td>
<td width="98%">PNS yang karena jabatannya tidak diberikan hak atas cuti bersama, hak cuti tahunannya ditambah sesuai dengan jumlah cuti bersama yang tidak diberikan.
<br/>
Contoh:
<br />
Sdri. Filda Rista, NIP. 198410042010122001 PNS yang menduduki jabatan fungsional perawat pada Rumah Sakit Umum Daerah Brebes. Pada bulan Juni tahun 2017 yang bersangkutan tidak diberikan hak cuti bersama dalam rangka Hari Raya Idul Fitri selama 5 (lima) hari kerja karena harus tugas jaga/piket. Dalam hal demikian, maka hak atas cuti tahunan tahun 2017 ditambah 5 (lima) hari kerja.
</td>
</tr>
<tr>
<td width="2%">5.</td>
<td width="98%">Penambahan hak atas cuti tahunan sebagaimana dimaksud pada angka 4 hanya dapat digunakan dalam tahun berjalan.</td>
</tr>
</table>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>	
            </div><!-- /.container-fluid -->
        </div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
	include("layout_bottom.php");
?>