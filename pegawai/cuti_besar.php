<?php
	include("sess_check.php");
	
	// deskripsi halaman
	$pagedesc = "Buat Pengajuan";
	$menuparent = "cuti";
	include("layout_top.php");
	$now = date('Y-m-d'); 
	$id_pegawai = $sess_pegawaiid;
?>
<script type="text/javascript">
function valid()
{ 
	if(document.cuti.tgl_akhir.value < document.cuti.tgl_awal.value){
		alert("Tanggal akhir harus lebih besar dari tanggal mulai cuti!");
		return false;
	}
	 
	return true;
}
</script>
<!-- top of file -->

	<script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
		<!-- Page Content -->
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Pengajuan Cuti Besar</h1>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->

  				<label>Lihat Peraturan Pengajuan Cuti Besar</label>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSaya">
  				 Lihat
				</button>
				<hr />
				<div class="row">
					<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal" name="cuti" action="cuti_insert.php" method="POST" enctype="multipart/form-data" onSubmit="return valid();">
							<div class="panel panel-default">
								<div class="panel-heading"><h3>Form Pengajuan Cuti Besar</h3></div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label col-sm-3">NIP</label>
										<div class="col-sm-4">
											<input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $id_pegawai;?>"readonly>
											<input type="text" name="nip" class="form-control" value="<?php echo $res['nip'];?>"readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Jenis Cuti</label>
										<div class="col-sm-4">
											<input type="text" name="jenis_cuti" class="form-control" value="Cuti Besar" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Mulai Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_awal" class="form-control" required>
											<input type="hidden" name="now" class="form-control" value="<?php echo $now;?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Akhir Cuti</label>
										<div class="col-sm-4">
											<input type="date" name="tgl_akhir" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Alamat Selama Menjalankan Cuti</label>
										<div class="col-sm-8">
											<input type="text" name="alm_cuti" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3">Keterangan</label>
										<div class="col-sm-8">
											<input type="text" name="keterangan" class="form-control" required>
										</div>
									</div>
									<input type="hidden" name="stt_cuti" class="form-control" value="Menunggu Persetujuan">
									<input type="hidden" name="ket_reject" class="form-control" value="">
									<input type="hidden" name="hrd_app" class="form-control" value="0">
								</div>
								<div class="panel-footer">
									<button type="submit" name="simpan" class="btn btn-success">Simpan</button>
								</div>
							</div><!-- /.panel -->
						</form>
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->

			        <!-- Modal -->
<div class="modal fade" id="modalSaya" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"  role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSayaLabel" align="center"><b>Peraturan Pengajuan Cuti Besar</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="justify">
<table width="100%" align="justify" class="table table-striped table-bordered table-hover">
<tr>
<td width="2%">1.</td>
<td width="98%">PNS yang telah bekerja paling singkat 5 (lima) tahun secara terus menerus berhak atas cuti besar paling lama 3 (tiga) bulan.</td>
</tr>
<tr>
<td width="2%">2.</td>
<td width="98%">PNS yang menggunakan hak atas cuti besar tidak berhak atas cuti tahunan dalam tahun yang bersangkutan.
<br />
Contoh:
<br />
Sdr. Aldi NIP. 198001122014011005 telah bekerja secara terus menerus sejak Januari 2014. Pada tanggal 10 Februari 2019 mengajukan permintaan cuti besar selama  3  (tiga)  bulan terhitung mulai 1 Maret 2019 sampai dengan 31 Mei 2019. Kemudian pada tanggal 18 Februari 2019 Pejabat Yang Berwenang Memberikan Cuti, memberikan cuti besar sesuai permintaan PNS yang bersangkutan.
<br />
Dalam hal demikian maka Sdr. Aldi:
<br />
a. Tidak berhak atas cuti tahunan untuk tahun 2019.
<br />
b. Cuti besar berikutnya baru dapat diajukan paling cepat 1 Juni 2024.
 </td>
</tr>
<tr>
<td width="2%">3.</td>
<td width="98%">PNS yang telah menggunakan hak atas cuti tahunan pada tahun yang bersangkutan maka hak atas cuti besar yang bersangkutan diberikan dengan memperhitungkan hak atas cuti tahunan yang telah digunakan.
<br />
Contoh:
<br />
Sdr. Ahmad NIP. 198501122014011009 telah bekerja secara terus menerus sejak 1 Januari 2014. Pada bulan Maret 2019 yang bersangkutan telah menggunakan hak atas cuti tahunan tahun 2019 selama 12 (dua belas) hari kerja. Pada tanggal 4 November 2019 mengajukan permintaan cuti besar selama 3 (tiga) bulan terhitung mulai 18 November 2019 sampai dengan 18 Februari 2020. Dalam hal Pejabat Yang Berwenang Memberikan Cuti akan memberikan cuti selama 3 (tiga) bulan maka:
<br />
a. Pejabat Yang Berwenang Memberikan Cuti, saat menetapkan pemberian cuti besar tetap mempertimbangkan cuti tahunan yang sudah digunakan selama 12 (dua belas) hari kerja sebelum mengajukan permintaan cuti besar.
<br />
b. Hak atas cuti besar Sdr. Ahmad diberikan paling lama terhitung mulai 18 November 2019 sampai dengan 31 Januari 2020.
<br />
c. Sdr. Ahmad masih mempunyai hak atas coti tahunan pada tahun 2020.
<br />
d. Cuti besar berikutnya baru dapat diajukan paling cepat 1 Februari 2025. </td>
</tr>
<tr>
<td width="2%">4.</td>
<td width="98%">PNS yang menggunakan hak atas cuti besar dan masih mempunyai sisa hak atas cuti tahunan tahun sebelumnya maka dapat menggunakan sisa hak atas cuti tahunan tersebut.
<br />
contoh:
<br />
Sdr. Dion Abdul Rauf NIP. 198504032012021007 telah bekerja secara terus menerus sejak 1 Februari 2012. Pada tahun 2017, yang bersangkutan memiliki hak cuti tahunan 2017 selama 11 (sebelas) hari dan sisa hak cuti tahunan tahun 20l6 selama 6 (enam) hari. Pada tanggal 28 Agustus 2017 mengajukan permintaan cuti besar selama 3 (tiga) bulan terhitung mulai 1 September 2017 sampai dengan 30 November 2017. Pejabat Yang Berwenang Memberikan Cuti dapat memberikan cuti besar secara penuh selama 3 (tiga) bulan. Dalam hal demikian, maka:
<br />
a. Sdr. Dion tidak berhak atas cuti tahunan dalam tahun 2017.
<br />
b. Sdr. Dion masih mempunyai hak atas sisa  cuti  tahunan tahun 2016 selama 6 (enam) hari.
<br />
c. Cuti besar berikutnya baru dapat diajukan paling cepat 1 Desember 2022. </td>
</tr>
<tr>
<td width="2%">5.</td>
<td width="98%">Ketentuan sebagaimana dimaksud pada angka 1, dikecualikan bagi PNS yang masa kerjanya belum 5 (lima) tahun untuk kepentingan agama, yaitu menunaikan ibadah haji pertama kali dengan melampirkan jadwal keberangkatan/ kelompok terbang (kloter) yang dikeluarkan oleh instansi yang bertanggung jawab dalam penyelenggaraan haji. </td>
</tr>
<tr>
<td width="2%">6.</td>
<td width="98%">Untuk menggunakan hak atas cuti besar sebagaimana dimaksud pada angka 1, PNS yang bersangkutan mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti. </td>
</tr>
<tr>
<td width="2%">7.</td>
<td width="98%">Berdasarkan permintaan secara tertulis sebagaimana dimaksud pada angka 6, Pejabat Yang Berwenang Memberikan Cuti memberikan cuti besar kepada PNS yang bersangkutan. </td>
</tr>
<tr>
<td width="2%">8.</td>
<td width="98%">Permintaan dan pemberian cuti besar sebagaimana dimaksud pada angka 6 dan angka 7 dibuat menurut contoh dengan menggunakan formulir sebagaimana tercantum dalam Anak Lampiran 1.b yang merupakan bagian tidak terpisahkan dari Peraturan Badan ini. </td>
</tr>
<tr>
<td width="2%">9.</td>
<td width="98%">Hak cuti besar dapat ditangguhkan penggunaannya oleh Pejabat Yang Berwenang Memberikan Cuti untuk paling lama 1 (satu) tahun apabila terdapat kepentingan dinas mendesak, kecuali untuk kepentingan agama.
<br />
Contoh:
<br />
Sdr. Arman NIP 198001122014011005 telah bekerja secara terus-menerus sejak Januari tahun 2014. Dalam bulan Maret 2019 ia mengajukan cuti besar selama 3 (tiga) bulan, tetapi oleh karena kepentingan dinas mendesak, pemberian cuti besar ditangguhkan selama 1  (satu)  tahun,  sehingga  yang bersangkutan diberikan cuti besar mulai 1 Maret sampai dengan 31 Mei 2020. Dalam hal demikian perhitungan hak atas cuti besar berikutnya bukan terhitung mulai bulan Juni 2025, tetapi terhitung mulai bulan Juni 2024. </td>
</tr>
<tr>
<td width="2%">10.</td>
<td width="98%">PNS yang menggunakan cuti besar kurang dari 3 (tiga) bulan, maka sisa cuti besar yang menjadi haknya hapus.
<br />
Contoh:
<br />
Sdr. Amir NIP 198101152010031005 telah bekerja secara terus menerus sejak 1 Maret 2010. Pada 10 Mei 20 17 yang bersangkutan mengajukan cuti besar selama 2 (dua)  bulan sampai dengan 10 Juli 2017. Dalam hal demikian maka sisa hak atas cuti besar selama 1 (satu) bulan menjadi hapus. Sdr. Amir baru dapat mengajukan cuti besar  berikutnya  setelah 10 Juli 2022. </td>
</tr>
<tr>
<td width="2%">11.</td>
<td width="98%">Selama menggunakan hak atas cuti besar, PNS yang bersangkutan menerima penghasilan PNS. </td>
</tr>
<tr>
<td width="2%">12.</td>
<td width="98%">Penghasilan sebagaimana dimaksud pada angka 11, terdiri atas gaji pokok, tunjangan keluarga, dan tunjangan pangan sampai dengan ditetapkannya Peraturan Pemerintah yang  mengatur gaji, tunjangan, dan fasilitas PNS. </td>
</tr>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>	
				
            </div><!-- /.container-fluid -->
        </div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
	include("layout_bottom.php");
?>