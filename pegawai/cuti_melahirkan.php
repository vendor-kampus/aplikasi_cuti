<?php
include("sess_check.php");

	// deskripsi halaman
$pagedesc = "Buat Pengajuan";
$menuparent = "cuti";
include("layout_top.php");
$now = date('Y-m-d'); 
$id_pegawai = $sess_pegawaiid;
?>
<script type="text/javascript">
	function valid()
	{ 
		if(document.cuti.tgl_akhir.value < document.cuti.tgl_awal.value){
			alert("Tanggal akhir harus lebih besar dari tanggal mulai cuti!");
			return false;
		}

		
		return true;
	}
</script>
<!-- top of file -->
<script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
<!-- Page Content -->
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Pengajuan Cuti Melahirkan</h1>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
		<label>Lihat Peraturan Pengajuan Cuti Melahirkan</label>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSaya">
			Lihat
		</button>
		<hr />
		<div class="row">
			<div class="col-lg-12"><?php include("layout_alert.php"); ?></div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" name="cuti" action="cuti_insert.php" method="POST" enctype="multipart/form-data" onSubmit="return valid();">
					<div class="panel panel-default">
						<div class="panel-heading"><h3>Form Pengajuan Cuti Besar</h3></div>
						<div class="panel-body">
							<div class="form-group">
								<label class="control-label col-sm-3">NIP</label>
								<div class="col-sm-4">
									<input type="hidden" name="id_pegawai" class="form-control" value="<?php echo $id_pegawai;?>"readonly>
									<input type="text" name="nip" class="form-control" value="<?php echo $res['nip'];?>"readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Jenis Cuti</label>
								<div class="col-sm-4">
									<input type="text" name="jenis_cuti" class="form-control" value="Cuti Melahirkan" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Mulai Cuti</label>
								<div class="col-sm-4">
									<input type="date" name="tgl_awal" class="form-control" required>
									<input type="hidden" name="now" class="form-control" value="<?php echo $now;?>" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Akhir Cuti</label>
								<div class="col-sm-4">
									<input type="date" name="tgl_akhir" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Alamat Selama Menjalankan Cuti</label>
								<div class="col-sm-8">
									<input type="text" name="alm_cuti" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-3">Keterangan</label>
								<div class="col-sm-8">
									<input type="text" name="keterangan" class="form-control" required>
								</div>
							</div>
							<input type="hidden" name="stt_cuti" class="form-control" value="Menunggu Persetujuan">
							<input type="hidden" name="ket_reject" class="form-control" value="">
							<input type="hidden" name="hrd_app" class="form-control" value="0">
						</div>
						<div class="panel-footer">
							<button type="submit" name="simpan" class="btn btn-success">Simpan</button>
						</div>
					</div><!-- /.panel -->
				</form>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->

		<div class="modal fade" id="modalSaya" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg"  role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="modalSayaLabel" align="center"><b>Peraturan Pengajuan Cuti Melahirkan</b></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" align="justify">
						<table width="100%" align="justify" class="table table-striped table-bordered table-hover">
							<tr>
								<td width="2%">1.</td>
								<td width="98%">Untuk kelahiran anak pertama sampai dengan kelahiran anak ketiga pada saat menjadi PNS berhak atas cuti melahirkan.</td>
							</tr>
							<tr>
								<td width="2%">2.</td>
								<td width="98%">Untuk kelahiran anak keempat dan seterusnya kepada PNS diberikan cuti besar.</td>
							</tr>
							<tr>
								<td width="2%">3.</td>
								<td width="98%">Cuti besar untuk kelahiran anak keempat dan seterusnya berlaku ketentuan sebagai berikut:
									<br />
									a. permintaan cuti tersebut tidak dapat ditangguhkan;
									<br />
									b. mengesampingkan ketentuan telah bekerja paling singkat 5 tahun secara terus-menerus; dan
									<br />
								c. lamanya cuti besar tersebut sama dengan lamanya cuti melahirkan.</td>
							</tr>
							<tr>
								<td width="2%">4.</td>
								<td width="98%">Lamanya cuti melahirkan sebagaimana dimaksud pada angka 1 adalah 3 (tiga) bulan.</td>
							</tr>
							<tr>
								<td width="2%">5.</td>
								<td width="98%">Untuk menggunakan hak atas cuti melahirkan sebagaimana dimaksud pada angka 1, PNS yang bersangkutan mengajukan permintaan secara tertulis kepada Pejabat Yang Berwenang Memberikan Cuti.</td>
							</tr>
							<tr>
								<td width="2%">6.</td>
								<td width="98%">Berdasarkan permintaan secara tertulis sebagaimana dimaksud pada angka 5, Pejabat Yang Berwenang Memberikan Cuti memberikan cuti melahirkan kepada PNS yang bersangkutan.</td>
							</tr>
							<tr>
								<td width="2%">7.</td>
								<td width="98%">Permintaan dan pemberian cuti melahirkan sebagaimana dimaksud pada angka 6 dan angka 7 dibuat menurut contoh dengan menggunakan formulir sebagaimana tercantum dalam Anak Lampiran 1.b yang merupakan bagian tidak terpisahkan dari Peraturan Badan ini.</td>
							</tr>
							<tr>
								<td width="2%">8.</td>
								<td width="98%">Dalam hal tertentu PNS dapat mengajukan permintaan cuti melahirkan kurang dari 3 (Tiga) bulan.</td>
							</tr>
							<tr>
								<td width="2%">9.</td>
								<td width="98%">Selama menggunakan hak cuti melahirkan, PNS yang bersangkutan menerima penghasilan PNS.
								</td>
							</tr>
							<tr>
								<td width="2%">10.</td>
								<td width="98%">Penghasilan sebagaimana dimaksud pada angka 9, terdiri atas gaji pokok, tunjangan keluarga, tunjangan pangan dan tunjangan jabatan sampai dengan ditetapkannya Peraturan Pemerintah yang mengatur gaji, tunjangan, dan fasilitas PNS.</td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>			

	</div><!-- /.container-fluid -->
</div><!-- /#page-wrapper -->
<!-- bottom of file -->
<?php
include("layout_bottom.php");
?>