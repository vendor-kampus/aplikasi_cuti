<?php
	include("../sess_check.php");

	include("../dist/function/format_tanggal.php");
	$no 	 = $_GET['no'];
	$sql = "SELECT cuti.*, employee.* FROM cuti, employee WHERE cuti.id_pegawai=employee.id_pegawai
			AND cuti.no_cuti ='$no'";
	$query = mysqli_query($conn,$sql);
	$result = mysqli_fetch_array($query);
	$tahun = substr($result['nip'], 8,4);
	$thn = date('Y'); 
	$masakerja = $thn - $tahun;
	// deskripsi halaman
	$pagedesc = "Cetak Form Cuti";
	$pagetitle = str_replace(" ", "_", $pagedesc) 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<title><?php echo $pagetitle ?></title>

	<link href="foto/logo/bkpsdm.png" rel="icon" type="images/x-icon">

	<!-- Bootstrap Core CSS -->
	<link href="../libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="../dist/css/offline-font.css" rel="stylesheet">
	<link href="../dist/css/custom-report.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="../libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- jQuery -->
	<script src="../libs/jquery/dist/jquery.min.js"></script>

<style>

p {
font-size:12px;
}
td {
  font-size:9px;
}
th {
  font-size:10px;
}
</style>
</head>

<body>


	<section id="body-of-report">
		<div class="container-fluid">
			<table width="100%"> 
			<tr>
			<td width="60%">&nbsp;</td>
			<td width="40%" align="center"><p>Soreang, <?php echo Indonesia2Tgl($result['tgl_pengajuan']);?>
			<br />
			Kepada
			<br />
			Yth. ............
			<br />
			di .....</p>
			</td>
			</tr>
			</table>
			<br />
			<h6 align="center"> FORMULIR PERMINTAAN DAN PEMBERIAN CUTI </h6>
		

			<table class="table table-bordered">
				<tr>
				<td colspan="4">I. DATA PEGAWAI</td>
				</tr>
				<tr>
					<td width="15%">Nama</td>
					<td width="50%"><?php echo $result['nama_emp'] ?></td>
					<td width="15%">NIP</td>
					<td width="20%"><?php echo $result['nip'] ?></td>
				</tr>
				<tr>
					<td>Jabatan</td>
					<td><?php echo $result['jabatan'] ?></td>
					<td>Masa Kerja</td>
					<td><?php echo $masakerja ?> Tahun</td>
				</tr>
				</tr>
				<tr>
				<td>Unit Kerja</td>
				<td colspan="3"><?php echo $result['unit_kerja'] ?></td>
				</tr>
			</table>

			<table class="table table-bordered"> 
				<tr>
				<td colspan="4">II. JENIS CUTI YANG DIAMBIL**</td>
				</tr>
				<tr>
					<td width="35%">1. Cuti Tahunan</td>
					<td width="15%" align="center">&#10004;</td>
					<td width="35%">2. Cuti Besar</td>
					<td width="15%">&nbsp;</td>
				</tr>
				<tr>
					<td width="35%">3. Cuti Sakit</td>
					<td width="15%">&nbsp;</td>
					<td width="35%">4. Cuti Melahirkan</td>
					<td width="15%">&nbsp;</td>
				</tr>
				<tr>
					<td width="35%">5. Cuti Karena Alasan Penting</td>
					<td width="15%">&nbsp;</td>
					<td width="35%">6. Cuti di Luar Tanggungan Negara</td>
					<td width="15%">&nbsp;</td>
				</tr>
				</tr>
			</table>

			<table class="table table-bordered">
				<tr>
					<td width="100%"> III. ALASAN CUTI</td>
				</tr>
				<tr>
					<td><?php echo $result['keterangan'];?></td>
				</tr>
			</table>

			<table class="table table-bordered">
				<tr>
				<td colspan="6">IV. LAMANYA CUTI</td>
				</tr>
				<tr>
					<td width="10%">SELAMA</td>
					<td width="20%"><?php echo $result['lama_cuti']?> (hari/ <s>bulan</s>/<s>tahun</s>)*</td>
					<td width="10%">Mulai Tanggal</td>
					<td width="15%"><?php echo Indonesia2Tgl($result['tgl_awal']);?></td>
					<td width="5%">s/d</td>
					<td width="15%"><?php echo Indonesia2Tgl($result['tgl_akhir']);?></td>
				</tr>
			</table>

			<table class="table table-bordered">
				<tr>
				<td colspan="7">V. CATATAN CUTI***</td>
				</tr>
				<tr>
					<td colspan="3">1. CUTI TAHUNAN</td>
					<td colspan="2">2. CUTI BESAR</td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="1">Tahun</td>
					<td colspan="1">Sisa</td>
					<td colspan="1">Keterangan</td>
					<td colspan="2">3. CUTI SAKIT</td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="1">N-2</td>
					<td colspan="1">-</td>
					<td colspan="1">-</td>
					<td colspan="2">4. CUTI MELAHIRKAN</td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="1">N-1</td>
					<td colspan="1">-</td>
					<td colspan="1">-</td>
					<td colspan="2">5. CUTI KARENA ALASAN PENTING</td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="1">N</td>
					<td colspan="1"><?php echo $result['jml_cuti']?></td>
					<td colspan="1"><?php echo $result['jml_cuti']?> Hari</td>
					<td colspan="2">3. CUTI DI LUAR TANGGUNGAN NEGARA</td>
					<td colspan="2">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table class="table table-bordered">
				<tr>
				<td colspan="3">VI. ALAMAT SELAMA MENJALANKAN CUTI</td>
				</tr>
				<tr width="100%">
					<td width="50%"><?php echo $result['alm_cuti']?></td>
					<td width="15%">TELP</td>
					<td width="35%"><?php echo $result['telp_emp']?></td>
				</tr>
				<tr>
					<td colspan="1">&nbsp;</td>
					<td colspan="2" align="center">
						Hormat Saya,
						<br />
						<br />
						<br />
						<u>( <?php echo $result['nama_emp']?> ) </u>
						<br />
						<?php echo $result['nip']?>
					</td>
			
				</tr>
			</table>
			<table class="table table-bordered">
				<tr>
				<td colspan="4">VII. PERTIMBANGAN ATASAN LANGSUNG</td>
				</tr>
				<tr  align="center">
					<td width="25%">DISETUJUI</td>
					<td width="25%">PERUBAHAN***</td>
					<td width="25%">DITANGGUHKAN****</td>
					<td width="25%">TIDAK DISETUJUI****</td>
				</tr>
				<tr align="center">
					<td>&#10004;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
					<td align="center">
						Hormat Saya,
						<br />
						<br />
						<u>( <?php echo $result['nama_ttd']?> ) </u>
						<br />
						<?php echo $result['nip_ttd']?>
					</td>
			</table>
			<table class="table table-bordered">
				<tr>
				<td colspan="4">VIII. KEPUTUSAN PBJABAT YANG BERWENANG MEMBERIKAN CUTI**</td>
				</tr>
				<tr  align="center">
					<td width="25%">DISETUJUI</td>
					<td width="25%">PERUBAHAN***</td>
					<td width="25%">DITANGGUHKAN****</td>
					<td width="25%">TIDAK DISETUJUI****</td>
				</tr>
				<tr align="center">
					<td>&#10004;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
					<td align="center" >
						Hormat Saya,
						<br />
						<br />
						<u>( 2 )</u>
						<br />
						2
					</td>
			</table>

			
		</div><!-- /.container -->
	</section>

	<script type="text/javascript">
		$(document).ready(function() {
			window.print();
		});
	</script>

	<!-- Bootstrap Core JavaScript -->
	<script src="../libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- jTebilang JavaScript -->
	<script src="../libs/jTerbilang/jTerbilang.js"></script>

</body>
</html>